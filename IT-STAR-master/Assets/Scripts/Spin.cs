﻿using UnityEngine;
using System.Collections;

public class Spin : MonoBehaviour {
	public int speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("spin");
		transform.Rotate (Vector3.forward, speed * Time.deltaTime);	
	}
}
