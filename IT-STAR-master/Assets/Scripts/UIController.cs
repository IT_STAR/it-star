﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

	public int waitTime;
	public GameObject repairCanvas;
	public GameObject aboutCanvas;
	public AudioSource soundPlayer;
	public Animator anim1;
	public Animator anim2;
	public TouchController touchController;




	// Use this for initialization
	void Start () {
	
		anim1.SetBool ("panelLeft", false);
		anim2.SetBool ("instructionsUp", false);

		if (Application.loadedLevel == 0) {
			waitTime = 100;
		} else {
			waitTime = 500;

		}


	}



	//----------------------------------------------------------------Button fuctions-------------------------------------------------------------------------------------------
	public void Quit(){
		Application.Quit ();
	}


	public void AboutOn(){
		aboutCanvas.SetActive (true);
		repairCanvas.SetActive (false);
	}

	//tutorial instructions
	public void RepairOn(){
		repairCanvas.SetActive (true);
		aboutCanvas.SetActive (false);

	}

	//back button functions
	public void Back(){
		repairCanvas.SetActive (false);
		aboutCanvas.SetActive (false);
		anim1.SetBool ("panelLeft", false);
		anim2.SetBool ("instructionsUp", false);
		touchController.HDOn = false;
		touchController.RAMOn = false;
		touchController.WiFiOn = false;
	}

	public void AudioClick1(){
		soundPlayer.Play();
	}



	
	// Update is called once per frame
	void Update () {
	
		if (touchController.HDOn == true) {
			repairCanvas = GameObject.Find ("Repair_HD");
			aboutCanvas = GameObject.Find ("About_HD");
		}
		if (touchController.cdOn == true) {
			repairCanvas = GameObject.Find ("Repair_CD");
			aboutCanvas = GameObject.Find ("About_CD");
		}
		if (touchController.RAMOn == true) {
			repairCanvas = GameObject.Find ("Repair_HD");
			aboutCanvas = GameObject.Find ("About_HD");
		}
		if (touchController.batteryOn == true) {
			repairCanvas = GameObject.Find ("Repair_Battery");
			aboutCanvas = GameObject.Find ("About_Battery");
		}
		if (touchController.WiFiOn == true) {
			repairCanvas = GameObject.Find ("Repair_WiFi");
			aboutCanvas = GameObject.Find ("About_WiFi");
		}
		if (touchController.fanOn == true) {
			repairCanvas = GameObject.Find ("Repair_Fan");
			aboutCanvas = GameObject.Find ("About_Fan");
		}





		waitTime -= 1;

		if (waitTime <= 0){
			Application.LoadLevel (Application.loadedLevel + 1);
		}

		if (Input.anyKey) {
			waitTime = -1;
		}

	}
}
