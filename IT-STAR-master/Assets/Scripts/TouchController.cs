﻿using UnityEngine;
using System.Collections;

public class TouchController : MonoBehaviour {


	public AudioSource sound;
	public Animator anim1;
	public Animator anim2;
	public Renderer hdRenderer;
	public Renderer ramRenderer;
	public Renderer cdRenderer;
	public Renderer fanRenderer;
	public Renderer batteryRenderer;
	public Renderer wifiRenderer;
	public bool HDOn;
	public bool RAMOn;
	public bool menuOn;
	public bool WiFiOn;
	public bool batteryOn;
	public bool cdOn;
	public bool fanOn;

	void Awake (){
		//get mesh renderer of model object
		hdRenderer = GameObject.Find("HD_Model").GetComponent<MeshRenderer>();
		ramRenderer = GameObject.Find("RAM_Model").GetComponent<MeshRenderer>();
		wifiRenderer = GameObject.Find("Wifi Adapter_Model").GetComponent<MeshRenderer>();
		cdRenderer = GameObject.Find("CD_Model").GetComponent<MeshRenderer>();
		fanRenderer = GameObject.Find("Fan_Model").GetComponent<MeshRenderer>();
		batteryRenderer = GameObject.Find("Battery_Model").GetComponent<MeshRenderer>();


		HDOn = false;
		RAMOn = false;
		menuOn = false;
		WiFiOn = false;
	}


	void Update () {

		//checks to see if any model has been selected
		if (HDOn || RAMOn || WiFiOn || batteryOn || cdOn || fanOn) {
			menuOn = true;
		}

		//turn on the model mesh renderer
		if (batteryOn == false) {
			batteryRenderer.enabled = false;
		} else {
			batteryRenderer.enabled = true;

		}

		if (cdOn == false) {
			cdRenderer.enabled = false;
		} else {
			cdRenderer.enabled = true;

		}

		if (fanOn == false) {
			fanRenderer.enabled = false;
		} else {
			fanRenderer.enabled = true;

		}


		if (HDOn == false) {
			hdRenderer.enabled = false;
		} else {
			hdRenderer.enabled = true;

		}

		if (RAMOn == false) {
			ramRenderer.enabled = false;
		} else {
			ramRenderer.enabled = true;

		}


		if (WiFiOn == false) {
			wifiRenderer.enabled = false;
		} else {
			wifiRenderer.enabled = true;

		}


		//get touch input
		Touch myTouch;

		Touch[] myTouches = Input.touches;

		for (int i = 0; i < Input.touchCount; i++) {
			myTouch = Input.GetTouch (i);
			if (myTouch.phase == TouchPhase.Began) {
				Ray ray = Camera.main.ScreenPointToRay (myTouch.position);
				RaycastHit hit;
				Physics.Raycast (ray, out hit);
				//touch hard drive
				if ((hit.rigidbody.gameObject.name == "HD_Raycast target") && (menuOn != true)){
					Debug.Log ("HD ON is true");
					anim1.SetBool ("panelLeft", true);
					anim2.SetBool ("instructionsUp", true);
					HDOn = true;
				}

				//touch ram 
				else if ((hit.rigidbody.gameObject.name == "RAM_Raycast target") && (menuOn != true)){
					anim1.SetBool ("panelLeft", true);
					anim2.SetBool ("instructionsUp", true);
					RAMOn = true;
				}
				else if ((hit.rigidbody.gameObject.name == "WiFi_Raycast target") && (menuOn != true)){
					anim1.SetBool ("panelLeft", true);
					anim2.SetBool ("instructionsUp", true);
					WiFiOn = true;
				}
				else if ((hit.rigidbody.gameObject.name == "Battery_Raycast target") && (menuOn != true)){
					anim1.SetBool ("panelLeft", true);
					anim2.SetBool ("instructionsUp", true);
					batteryOn = true;
				}
				else if ((hit.rigidbody.gameObject.name == "CD_Raycast target") && (menuOn != true)){
					anim1.SetBool ("panelLeft", true);
					anim2.SetBool ("instructionsUp", true);
					cdOn = true;
				}
				else if ((hit.rigidbody.gameObject.name == "Fan_Raycast target") && (menuOn != true)){
					anim1.SetBool ("panelLeft", true);
					anim2.SetBool ("instructionsUp", true);
					fanOn = true;
				}


			}
		}
	}
}
