﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnableComp : MonoBehaviour {

	public Button about;
	public Button tutorial;
	public Button assessment;
	public Button back; 
	public Button left;
	public Button right;

	public Image abouti;
	public Image tutoriali;
	public Image assessmenti;
	public Image backi; 
	public Image lefti;
	public Image righti;


	public void removebutt () {
		
		about = GameObject.Find ("About").GetComponent<Button> ();
		tutorial = GameObject.Find ("Repair").GetComponent<Button> ();
		assessment = GameObject.Find ("Assessment").GetComponent<Button> ();
		back = GameObject.Find ("Back").GetComponent<Button> ();
		left = GameObject.Find ("ButtonRight").GetComponent<Button> ();
		right = GameObject.Find ("ButtonLeft").GetComponent<Button> ();
		abouti = GameObject.Find ("About").GetComponent<Image> ();
		tutoriali = GameObject.Find ("Repair").GetComponent<Image> ();
		assessmenti = GameObject.Find ("Assessment").GetComponent<Image> ();
		backi = GameObject.Find ("Back").GetComponent<Image> ();
		lefti = GameObject.Find ("ButtonRight").GetComponent<Image> ();
		righti = GameObject.Find ("ButtonLeft").GetComponent<Image> ();
		about.interactable = false; 
		tutorial.interactable = false;
		assessment.interactable = false;
		left.interactable = true;
		right.interactable = true;
		abouti.enabled = false; 
		tutoriali.enabled = false;
		assessmenti.enabled = false;
		lefti.enabled = true;
		righti.enabled = true;

	}
	public void bringbutt () {

//		about = GameObject.Find ("About").GetComponent<Button> ();
//		tutorial = GameObject.Find ("Repair").GetComponent<Button> ();
//		assessment = GameObject.Find ("Assessment").GetComponent<Button> ();
//		back = GameObject.Find ("Back").GetComponent<Button> ();
//		left = GameObject.Find ("ButtonRight").GetComponent<Button> ();
//		right = GameObject.Find ("ButtonLeft").GetComponent<Button> ();
//		abouti = GameObject.Find ("About").GetComponent<Image> ();
//		tutoriali = GameObject.Find ("Repair").GetComponent<Image> ();
//		assessmenti = GameObject.Find ("Assessment").GetComponent<Image> ();
//		backi = GameObject.Find ("Back").GetComponent<Image> ();
//		lefti = GameObject.Find ("ButtonRight").GetComponent<Image> ();
//		righti = GameObject.Find ("ButtonLeft").GetComponent<Image> ();
		about.interactable = true; 
		tutorial.interactable = true;
		assessment.interactable = true;
		left.interactable = false;
		right.interactable = false;
		abouti.enabled = true; 
		tutoriali.enabled = true;
		assessmenti.enabled = true;
		lefti.enabled = false;
		righti.enabled = false;

	}

	}

