﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class removetext : MonoBehaviour {

	public Text Atext; //about
	public Text Rtext;
	public Text Btext; // use this maybe 
	public Text ITtext;
	public Text Asstext; 

	public void deletetext(){
		Atext =GameObject.Find ("Atext").GetComponent<Text>();
		Atext.enabled = false;
		Rtext =GameObject.Find ("Rtext").GetComponent<Text>();
		Rtext.enabled = false;
		Asstext =GameObject.Find ("Asstext").GetComponent<Text>();
		Asstext.enabled = false;
		ITtext =GameObject.Find ("ITtext").GetComponent<Text>();
		Atext.enabled = false;

	}

	public void bringtext(){
		Atext =GameObject.Find ("Atext").GetComponent<Text>();
		Atext.enabled = true;
		Rtext =GameObject.Find ("Rtext").GetComponent<Text>();
		Rtext.enabled = true;
		Asstext =GameObject.Find ("Asstext").GetComponent<Text>();
		Asstext.enabled = true;
		ITtext =GameObject.Find ("ITtext").GetComponent<Text>();
		Atext.enabled = true;

	}
		
}
